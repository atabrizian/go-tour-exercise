package main

import (
	"code.google.com/p/go-tour/wc"
	"strings"
)

func WordCount(s string) map[string]int {
	fields := strings.Fields(s)
	mymap := make(map[string]int, len(fields))

	for _, value := range fields {
		mymap[value] = mymap[value]+1
	}
	return mymap
}

func main() {
	wc.Test(WordCount)
}
