package main

import "fmt"

// fibonacci is a function that returns
// a function that returns an int.
func fibonacci() func() int {
	step := 0
	previousNum := 0
	num := 1
	result := 0

	return func() int {
		if step >= 2 {
			result = num + previousNum
			previousNum = num
			num = result
		} else {
			result = step
			step++
		}

		return result
	}
}

func main() {
	f := fibonacci()

	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
