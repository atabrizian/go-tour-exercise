package main

import (
	"code.google.com/p/go-tour/pic"
)

type My struct {
	x int
	y int
}

func Pic(dx, dy int) [][]uint8 {
    pic := make([][]uint8, dy)
    for y := range pic {

        pic[y] = make([]uint8, dx)

        for x := range pic[y] {
            pic[y][x] = uint8((x*y + 4)/2)
        }
    }

    return pic
}

func main() {
	pic.Show(Pic)
}
